package com.graphisigner.nosepicker_test12_mad5244_naveen_c0730128;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

public class MainActivity extends AppCompatActivity {


    GameEngine nosePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        nosePicker = new GameEngine(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(nosePicker);
    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        nosePicker.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        nosePicker.pauseGame();
    }
}
