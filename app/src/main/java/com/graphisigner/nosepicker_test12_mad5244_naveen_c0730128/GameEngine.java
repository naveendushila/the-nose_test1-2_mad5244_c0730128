package com.graphisigner.nosepicker_test12_mad5244_naveen_c0730128;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {


    // Android debug variables
    final static String TAG = "NOSEPICKER";

    // screen size
    int screeenFullWidth;
    int screenHeight;
    int screenWidth;
    int minX;
    int maxX;

    // game state
    boolean gameIsRunning;



    // threading
    Thread gameThread;

    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;
    boolean showHitboxes;
    boolean showBoundry;
    // ## SPRITES

   Nose nose;
   Finger finger;

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int scoreHit = 0;
    int scoreMiss = 0;

    public GameEngine(Context context, int w, int h) {
        super(context);
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        screeenFullWidth = w;

        int margin = (int) (w * 0.15);

        minX = margin;
        maxX = w - margin;

        this.screenWidth = w - (margin * 2);
        this.screenHeight = h;
        Log.d("Screen Margins","Screen Full Width" + screeenFullWidth);
//        Log.d("Screen Margins","Margins" + margin);
//        Log.d("Screen Margins","minX" + minX);
//        Log.d("Screen Margins","maxX" + maxX);
//        Log.d("Screen Margins","Screen Width after margin" + screenWidth);
//        Log.d("Screen Margins","Screen Height" + screenHeight);

        //@TODO: Adjust the screen and print the boundry
        showHitboxes = true;
        showBoundry = true;

        // @TODO: Add sprites (More enemies into the level of player)
        this.spawnFinger();
        this.spawnNose();

    }

    private void spawnFinger() {
        //@TODO: Finger play
        finger = new Finger(this.getContext(), minX, this.screenHeight - 600  );

    }


    private void spawnNose() {

        //Spawning Moving object
        nose = new Nose(this.getContext(), this.screenWidth / 2, 0);

    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {

       // @TODO: Update position fingers
        this.finger.updateFinger();

       if (finger.getxPosition() <= minX) {
           this.finger.setDirection(1);

        }
        else if (finger.getxPosition() + finger.getWidth() >= maxX) {
           this.finger.setDirection(0);
       }
       else {}


        // @TODO: Collision detection between Finger and nose
        if (this.finger.getHitbox().intersect(this.nose.getHitboxLeft()) || this.finger.getHitbox().intersect(this.nose.getHitboxLeft()) )
        {
            scoreHit ++ ;
            Log.d("Score", "Score: " + scoreHit);

        }
        else {}

         //@TODO: Collision detection between tank and enemy Fire Hydrant
       if (this.finger.getyPosition() <= 0) {
            scoreMiss ++;
            Log.d("Score","Finger Touch the top");
            try {
                gameThread.sleep(1000);
            } catch (Exception e) {

            }

        }
        else {
            //Do Nothing
       }


    }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();


            //@TODO: Show the background
            this.canvas.drawColor(Color.argb(255,212,255,226));

            //@TODO: Draw the nose
           canvas.drawBitmap(this.nose.getBitmap(), this.nose.getxPosition(), this.nose.getyPosition(), paintbrush);

            //@TODO: Draw the finger
           canvas.drawBitmap(this.finger.getBitmap(), this.finger.getxPosition(), this.finger.getyPosition(), paintbrush);


            //@TODO Show hitbox according to the variable


            if (showHitboxes == true) {
                paintbrush.setColor(Color.RED);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(5);

                Rect noseHitboxLeft = nose.getHitboxLeft();
                Rect noseHitboxRight = nose.getHitboxRight();

                Rect fingerHitbox = finger.getHitbox();

               canvas.drawRect(noseHitboxLeft.left, noseHitboxLeft.top, noseHitboxLeft.right, noseHitboxLeft.bottom, paintbrush);
                canvas.drawRect(noseHitboxRight.left, noseHitboxRight.top, noseHitboxRight.right, noseHitboxRight.bottom, paintbrush);
              canvas.drawRect(fingerHitbox.left, fingerHitbox.top, fingerHitbox.right, fingerHitbox.bottom, paintbrush);

            }

            if (showBoundry == true) {
                paintbrush.setColor(Color.GREEN);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(5);
                canvas.drawRect(minX, 0, maxX , screenHeight,paintbrush);
            }

            //@TODO: Show scores
            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.BLACK);
            canvas.drawText("Hits: " + scoreHit + " Miss: " + scoreMiss , 100, 800, paintbrush);


            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(60);
        } catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //@TODO: Jump the finger

        int userAction = event.getActionMasked();
          if (userAction == MotionEvent.ACTION_DOWN) {
              finger.jump();
            }


        return true;
    }
}
