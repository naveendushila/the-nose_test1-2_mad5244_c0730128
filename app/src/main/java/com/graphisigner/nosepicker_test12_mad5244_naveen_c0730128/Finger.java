package com.graphisigner.nosepicker_test12_mad5244_naveen_c0730128;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Finger {

    // threading
    Thread jumpThread;

    int direction = 0;
    int xPosition;
    int yPosition;

    int fingerSpeed = 30;
    int fingerJump = 15;


    Bitmap finger;

    int height;
    int width;

    private Rect hitBox;

    public Finger(Context context, int x, int y) {
        this.finger = BitmapFactory.decodeResource(context.getResources(), R.drawable.finger01);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.finger.getWidth(), this.yPosition + this.finger.getHeight());
    }


    public void updateFinger() {

        if (this.direction == 0) {
            this.xPosition = this.xPosition - fingerSpeed;
        }
        else {
            this.xPosition = this.xPosition + fingerSpeed;
        }
        // update the position of the hitbox of finger 1
//        this.hitBox.left = this.xPosition;
//        this.hitBox.right = this.xPosition + this.finger.getWidth();
        this.updateHitbox();
    }

    public void updateHitbox() {
        // update the position of the hitbox of finger 1
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.finger.getWidth();
        this.hitBox.bottom = this.yPosition + this.finger.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }


    public void setxPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
    }

    public void setyPosition(int y) {
        this.yPosition = y;

        this.updateHitbox();
    }

    public int getxPosition()
    {
        return this.xPosition;

    }
    public int getyPosition() {
        return this.yPosition;
    }

    public Bitmap getBitmap() {
        return this.finger;
    }

    public int getHeight() {
        return this.finger.getHeight();
    }
    public int getWidth(){
        return this.finger.getWidth();
    }
    public void setDirection(int i) {
        this.direction = i;
    }
 public void jump() {
        int intial = this.yPosition;
        while (this.yPosition > 0) {
            this.yPosition = this.yPosition - fingerJump;
            this.updateHitbox();
        }
        try {
        jumpThread.sleep(100);
        } catch (Exception e) {

        }
        while (this.yPosition < intial) {
            this.yPosition = this.yPosition + 2 ;
            this.updateHitbox();
        }

 }

}
