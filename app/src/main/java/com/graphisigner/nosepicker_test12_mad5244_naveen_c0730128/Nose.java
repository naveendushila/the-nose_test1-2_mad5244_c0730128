package com.graphisigner.nosepicker_test12_mad5244_naveen_c0730128;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

public class Nose {


    int xPosition;
    int yPosition;

    Bitmap nose;

    int height;
    int width;

    private Rect hitBoxLeft;
    private Rect hitBoxRight;


    public Nose(Context context, int x, int y) {
        this.nose = BitmapFactory.decodeResource(context.getResources(), R.drawable.nose01);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBoxLeft = new Rect(this.xPosition + 150, this.yPosition + 500 , this.xPosition + 150 + 225, this.yPosition + 650);
        this.hitBoxRight = new Rect(this.xPosition + 450, this.yPosition + 500 , this.xPosition + 450 + 225, this.yPosition + 650);
    }

    public Rect getHitboxLeft() {
        return this.hitBoxLeft;
    }

    public Rect getHitboxRight() {
        return this.hitBoxRight;
    }


    public Bitmap getBitmap() {
        return this.nose;
    }

    public int getHeight() {
        return this.nose.getHeight();
    }

    public int getWidth(){
        return this.nose.getWidth();
    }

    public void setxPosition(int x) {
        this.xPosition = x;
    }

    public void setyPosition(int y) {
        this.yPosition = y;
    }

    public int getxPosition()
    {
        return this.xPosition;

    }
    public int getyPosition() {
        return this.yPosition;
    }

}
